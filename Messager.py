import asyncio

from aiogram import Bot, Dispatcher, types
from aiogram.types import Message

from aiogram.utils.exceptions import BotBlocked

import time

from progress_bar import InitBar

'''Usage:

asyncio.run(Messenger.send_message(message = "...", bots = {"<bot token>":[<chat id 1>, <chat id 2>, ...], "<...>:[...]} ))

'''

class Messenger:

    #Dealing with Telegrams 20/min limit per bot per chat_id

    #stored bot/chat_id pairs with times and amounts they were sent to
    # in the form: { "bot_token":{chat_id1:time_start_1, chat_id2:time_start_2, ...} }
    bot_chat_id_times = {}
    #time_start resets when if 1 minute passes
    #if a minute has not passed, wait 3 seconds then send message. That way max 20 messages will
    #always fit into 1 minute 

    #The method that sends messages, takes Telegram limits into account and returnes message statistics
    #bots is a dictionary with the bots token as keys, and lists of chat ids in a list as items
    async def send_message(message = None, bots = {}):

        errors = 0
        blocked = 0
        success = 0

        # progress bar
        pbar = InitBar()

        # Going through the dictionary to see amount of items we need to go through for progress bar:
        progress_chunks = 0
        for bot_token in bots:
            for chat_id in bots[bot_token]:
                progress_chunks += 1

        # So we never divide by 0: if this number is zero notheing gets sent,
        # so progress is automatically 100%
        if progress_chunks == 0:
            progress_chunks = 1

        percentage_one_chat_id = int(100 / progress_chunks) # using int to round down and get int value
        current_progress_bar_percentage = 0

        #recording start time for calculating speed
        function_start_time = time.time()

        #going through the dictionary
        for bot_token in bots:

            #to account for Telegrams 30/sec limit per bot
            message_count = 0

            #creating bot object from token
            current_bot = Bot(bot_token)

            #going through all chat_ids bot wants to send to
            for chat_id in bots[bot_token]:

                #First to account for Telegrams 20/min limit per bot per chat_id:

                #start dict if it is not there, or if this chat is not there
                if bot_token not in Messenger.bot_chat_id_times:
                    Messenger.bot_chat_id_times[bot_token] = {chat_id:time.time()}
                else:
                    if chat_id not in Messenger.bot_chat_id_times[bot_token]:
                        Messenger.bot_chat_id_times[bot_token][chat_id] = time.time()
                
                #Then if difference between times is < 60 seconds, wait 3 seconds then send.
                #That way max 20 messages will always fit into 1 minute. 
                #Otherwise continue as normal, and update the time to be current time
                print(Messenger.bot_chat_id_times)
                if time.time() - Messenger.bot_chat_id_times[bot_token][chat_id] >= 60:
                    #print(Messenger.bot_chat_id_times[bot_token][chat_id])
                    #print("=======new_time==========")
                    Messenger.bot_chat_id_times[bot_token][chat_id] = time.time()
                    #print(Messenger.bot_chat_id_times[bot_token][chat_id])
                else:
                    await asyncio.sleep(3.158) #For cases where extra buffer is needed, 60/19 = 3.1578
                    #print(Messenger.bot_chat_id_times[bot_token][chat_id])

                #Update progress bar
                current_progress_bar_percentage += percentage_one_chat_id
                pbar(current_progress_bar_percentage)  # update % to correct amount

                #Now to account for Telegrams 30/sec limit

                #if count is over limit, wait one second and reset count.
                #This makes sure there will never be more than 30 messages per second
                if message_count >= 30:
                    await asyncio.sleep(1)
                    message_count = 0
                #then try sending message
                try:
                    this_message = await current_bot.send_message(chat_id, message)
                    success += 1
                except BotBlocked: #error occurs when bot is blocked
                    blocked += 1
                except Exception as e: #any other errors we encounter
                    errors += 1
                    print(e)
                
                #increase message count
                message_count += 1
        
        #Update progress bar to 100%
        pbar(100) 
        await asyncio.sleep(0.1) #so that you can see the bar

        # Delete the progress bar
        del pbar  

        #return statement
        return {"Amount of messages: ":success+blocked+errors, 
                "Speed of messages: ":(success+blocked+errors)/(time.time()-function_start_time),
                "Successfull messages":success,
                "Blocked messages":blocked,
                "Failed messages":errors}

        

#For testing
#asyncio.run(Messenger.send_message(message = "Hello1", bots = {"...":["...", "..."], ...} ))
#asyncio.run(Messenger.send_message(message = "Hello2", bots = {"...":["..."], ...} ))
